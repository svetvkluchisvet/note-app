import { AiFillDelete, AiFillEdit } from 'react-icons/ai'
import './note.css'

export const Note = ({ note, note: { id, text, date }, handleDeleteNote, captureEdit, changeEditState }) => {
  return (
        <div key={id} className="note">
            <span className="text">{text}</span>
            <div className="note-footer">
                <small>{date}</small>
                <div>
                    <AiFillDelete
                        onClick={() => handleDeleteNote(id)}
                        className="delete-icon"
                        size="1.3em"
                    />
                    <AiFillEdit
                        onClick={() => {
                          captureEdit(note)
                          changeEditState(note)
                        }}
                        className="delete-icon"
                        size="1.3em"
                    />
                </div>
            </div>
        </div>
  )
}
