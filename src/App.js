import { useEffect, useState } from 'react'
import { nanoid } from 'nanoid'
import { NotesList } from './components/NoteList/NotesList'
import { Search } from './components/Search/Search'
import { Header } from './components/Header/Header'
import axios from 'axios'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'react-i18next'
import { Loader } from './components/Loader/Loader'

const apiUrl = 'http://localhost:3000/notes/'

const App = () => {
  const [notes, setNotes] = useState([])

  const [searchText, setSearchText] = useState('')

  const [darkMode, setDarkMode] = useState(false)

  const [isLoading, setIsLoading] = useState(true)

  const { t } = useTranslation()

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 2500)
  })

  useEffect(() => {
    const savedNotes = JSON.parse(
      window.localStorage.getItem('notes-app-data')
    )
    if (savedNotes) {
      setNotes(savedNotes)
    }
  }, [])

  useEffect(() => {
    window.localStorage.setItem(
      'notes-app-data',
      JSON.stringify(notes)
    )
  }, [notes])

  useEffect(() => {
    axios.get(apiUrl)
      .then((res) => {
        setNotes(res.data)
      })
  }, [])

  const addNote = (text) => {
    const date = new Date()
    axios.post(apiUrl, {
      id: nanoid(),
      text,
      date: date.toLocaleDateString()
    }).then((res) => {
      const newNotes = [...notes, res.data]
      setNotes(newNotes)
    })
  }

  const deleteNote = async (id) => {
    await axios.delete(apiUrl + id)
      .then((res) => {
        const newNotes = notes.filter((note) => note.id !== id)
        setNotes(newNotes)
      })

    toast(t('app.toast_note_deleted'), {
      position: 'top-right',
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: 'light',
      type: 'success'
    })
  }

  const updateNote = (updatedNote) => {
    const updatedNotes = notes.map((note) => {
      if (note.id === updatedNote.id) {
        return updatedNote
      } else {
        return note
      }
    })
    setNotes(updatedNotes)
  }

  return (
        <div className={`${darkMode && 'dark-mode'}`}>
            {isLoading === true
              ? <Loader/>
              : <div className="container">
                    <Header handleToggleDarkMode={setDarkMode}/>
                    <ToastContainer/>
                    <Search handleSearchNote={setSearchText}/>
                    <NotesList
                        notes={notes.filter((note) =>
                          note.text.toLowerCase().includes(searchText)
                        )}
                        handleAddNote={addNote}
                        handleDeleteNote={deleteNote}
                        handleUpdateNote={updateNote}
                    />
                </div>
            }
        </div>
  )
}
export default App
