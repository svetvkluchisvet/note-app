import { Note } from '../Note/Note'
import { AddNote } from '../AddNote/AddNote'
import { EditNote } from '../EditNote/EditNote'
import { useState } from 'react'
import './noteList.css'

export const NotesList = ({ notes, handleAddNote, handleDeleteNote, handleUpdateNote }) => {
  const [isEditing, setIsEditing] = useState(false)

  const [editForm, setEditForm] = useState({
    id: '',
    text: '',
    date: ''
  })

  const handleUpdateNotes = (updatedNote) => {
    setIsEditing(false)
    handleUpdateNote(updatedNote)
  }

  const handleNonUpdateNotes = () => {
    setIsEditing(false)
    handleUpdateNote(notes)
  }

  const handleChange = (e) => {
    setEditForm({
      ...editForm,
      [e.target.name]: e.target.value
    })
  }

  const changeEditState = (note) => {
    console.log(note.id)
    if (note.id === editForm.id) {
      setIsEditing(isEditing => !isEditing)
    } else if (isEditing === false) {
      setIsEditing(isEditing => !isEditing)
    }
  }

  const captureEdit = (clickedNote) => {
    const filter = notes.filter(note => note.id === clickedNote.id)
    setEditForm(filter[0])
  }

  return (
        <div key={notes.id} className="notes-list">
            {isEditing
              ? (<EditNote
                    editForm={editForm}
                    handleChange={handleChange}
                    handleUpdateNotes={handleUpdateNotes}
                    handleNonUpdateNotes={handleNonUpdateNotes}
                />)
              : null}
            <AddNote handleAddNote={handleAddNote}/>
            {notes.map((note) => (
                <Note
                    key={note.id}
                    note={note}
                    handleDeleteNote={handleDeleteNote}
                    captureEdit={captureEdit}
                    changeEditState={changeEditState}
                />

            ))}
        </div>
  )
}
