import axios from 'axios'
import { toast } from 'react-toastify'
import { useTranslation } from 'react-i18next'
import './editNote.css'

const apiUrl = 'http://localhost:3000/notes/'

export const EditNote = ({ editForm, handleUpdateNotes, handleChange, handleNonUpdateNotes, note }) => {
  const { id, text } = editForm
  const { t } = useTranslation()
  const characterLimit = 350

  const handleEditForm = async () => {
    if (text.length > 0) {
      await axios.patch(apiUrl + id, editForm)
        .then(
          handleUpdateNotes(editForm)
        )
        .catch(res =>
          console.log(res.message))

      toast(t('edit_note.toast_note_edited'), {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
        type: 'success'
      })
    } else {
      await axios.patch(apiUrl + id, note)
        .then(
          handleNonUpdateNotes()
        )
        .catch(res =>
          console.log(res.message))

      toast(t('add_note.toast_empty_note_notification'), {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
        type: 'warning'
      })
    }
  }

  return (
        <div className="note edit">
            <form>
                <textarea className="note edit" name="text" value={text} onChange={(e) => handleChange(e)}
                />
                <div className="note-footer">
                    <small>{characterLimit - text.length} Remaining</small>
                    <button type="submit" className="save" onClick={handleEditForm}>Save</button>
                </div>
            </form>
        </div>
  )
}
