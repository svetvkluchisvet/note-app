import { useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useTranslation } from 'react-i18next'
import './addNote.css'

export const AddNote = ({ handleAddNote }) => {
  const [noteText, setNoteText] = useState('')

  const { t } = useTranslation()

  const characterLimit = 350
  const handleChange = (event) => {
    if (characterLimit - event.target.value.length >= 0) {
      setNoteText(event.target.value)
    }
  }

  const handleSaveClick = () => {
    if (noteText.trim().length > 0) {
      handleAddNote(noteText)
      setNoteText('')
      toast(t('add_note.toast_note_saved'), {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
        type: 'success'
      })
    } else {
      toast(t('add_note.toast_empty_note_notification'), {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
        type: 'warning'
      })
    }
  }

  return (
        <div className="note new">
            <textarea
                rows='8'
                cols='10'
                placeholder={t('add_note.add_note_here')}
                onChange={handleChange}
                value={noteText}
            >
            </textarea>
            <div className="note-footer">
                <small>{characterLimit - noteText.length} {t('add_note.remaining')}</small>
                <button
                    className="save"
                    onClick={handleSaveClick}>{t('add_note.save')}
                </button>
            </div>
        </div>

  )
}
