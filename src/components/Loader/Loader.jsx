import { Hearts } from 'react-loader-spinner'
import './loader.css'

export const Loader = () => {
  return (
        <div className="parent">
            <div className="block">
                <Hearts
                    visible={true}
                    color="#f69ebb"
                    height="700"
                    width="700"
                    timeout={5000}
                    ariaLabel="blocks-loading"
                    wrapperStyle={{}}
                    wrapperClass="blocks-wrapper"
                />
            </div>
        </div>
  )
}
