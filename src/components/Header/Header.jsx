import { useTranslation } from 'react-i18next'
import './header.css'

export const Header = ({ handleToggleDarkMode }) => {
  const { t, i18n } = useTranslation()

  const changeLanguage = (language) => {
    i18n.changeLanguage(language)
  }

  return (
        <div className="header">
            <h1>{t('header.app_name')}
            </h1>
            <div className="button-container">
                <button
                    onClick={() =>
                      handleToggleDarkMode(
                        (previousDarkMode) => !previousDarkMode
                      )
                    }
                    className="save language"
                >{t('header.change_theme')}
                </button>
                <div className="language-button-container">
                    <button
                        className="save language"
                        onClick={() => changeLanguage('en')}
                    >
                        {t('header.en')}
                    </button>
                    <button
                        className="save language"
                        onClick={() => changeLanguage('ru')}
                    >
                        {t('header.ru')}
                    </button>
                </div>
            </div>
        </div>
  )
}
