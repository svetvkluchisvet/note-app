# Привет!!!

## Добавьте зависимости в проект 

Перейдите в корневую папку и запустите

### `npm install`
(если используете пакетный менеджер npm)

или
### `yarn install`
(если используете пакетный менеджер yarn)

## Активируйте eslint (не обязательно)

#### Для WebStorm и продуктов от JetBrains 
Нажмите CTRL+ALT+S (или File->Settings в левом верхнем углу), зайдите в настройки, неберите в поиске ESLint, сделайте его активным.

Выберите пункт "Automatic ESLint configuration"

Run for files "{**/*,*}.{js,ts,jsx,tsx,html}" //ts и tsx не обязательны для данного проекта

#### Для vscode

[https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

## После установки запустите json-server на 3000 порту (важно именно на 3000)

###  `npm run watch`
(json-server --watch db.json)

Откройте [http://localhost:3000/notes](http://localhost:3000/notes) чтобы посмотреть данные

## Далее запустите приложение на 3001 порту

###  `npm start`

## Все работает:)